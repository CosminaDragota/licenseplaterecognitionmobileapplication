package com.example.clientretrofit.models;

import com.google.gson.annotations.Expose;

public class LicenseNumber {

    @Expose
    private int Id;
    @Expose
    private String RecognizedNumber;
    @Expose
    private String CorrectedNumber;

    public void setRecognizedNumber(String recognizedNumber) {
        RecognizedNumber = recognizedNumber;
    }

    public void setCorrectedNumber(String correctedNumber) {
        CorrectedNumber = correctedNumber;
    }
}
