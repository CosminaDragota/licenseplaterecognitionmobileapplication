package com.example.clientretrofit.api;

import com.example.clientretrofit.models.LicenseNumber;
import com.example.clientretrofit.models.Response;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface WebApiClient {

    @Multipart
    @POST("api/FileUpload")
    Call<Response> uploadImage(@Part MultipartBody.Part image);

    @GET("api/ReadLicenseNumber")
    Call<String> getLicenseNumber();

    @POST("api/LicenseNumbers")
    Call<LicenseNumber> postLicenseNumber(@Body LicenseNumber licenseNumber);

    @GET("api/ShowImage")
    Call<ResponseBody> getImage();

    @POST("api/ShowImage")
    Call<String> analizeImage(@Body String imageName);

}
