package com.example.clientretrofit.api;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceGenerator {
    private static final String BASE_URL = "http://172.20.10.11/Server2/";
    //IP acasa:192.168.0.103
    //IP hotspot: 172.20.10.11

    private static Retrofit retrofit;

    public static Retrofit getWebApiClient(){
        if(retrofit==null){
            retrofit =new Retrofit.Builder().baseUrl(BASE_URL)
                    .addConverterFactory(new NullOnEmptyConverterFactory())
                    .addConverterFactory(GsonConverterFactory.create()).build();
        }
        return retrofit;
    }
}
