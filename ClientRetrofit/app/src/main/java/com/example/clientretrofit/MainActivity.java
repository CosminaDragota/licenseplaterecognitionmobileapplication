package com.example.clientretrofit;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.clientretrofit.api.ServiceGenerator;
import com.example.clientretrofit.api.WebApiClient;
import com.example.clientretrofit.models.LicenseNumber;
import com.example.clientretrofit.models.Response;
import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class MainActivity extends AppCompatActivity{


    public static final String TAG = MainActivity.class.getSimpleName();

    private static final int UPLOAD_REQUEST_CODE = 100;
    private static final int TAKE_PICTURE_REQUEST_CODE = 101;

    private Button btnSelectImage;
    private Button btnUpload;
    private Button btnTakePicture;
    private Button btnShowLicenseNumber;
    private Button btnStoreData;
    private Button btnDisplayLicensePlate;
    private Button btnAnalizeImage;
    private EditText ImgTitle;
    private EditText correctedLicenseNumber;
    private TextView generatedLicenseNumber;
    private ImageView Img;
    private Bitmap bitmap;
    private Uri path;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
    }

    private void initViews() {

        btnSelectImage = findViewById(R.id.btn_select_image);
        ImgTitle = findViewById(R.id.image_title);
        Img = findViewById(R.id.image_view);
        Glide.with(this)
                .load("http://wiki.tripwireinteractive.com/images/4/47/Placeholder.png")
                .into(Img);

        btnUpload = findViewById(R.id.btn_upload_image);
        btnUpload.setEnabled(false);

        btnTakePicture=findViewById(R.id.btn_take_picture);

        generatedLicenseNumber =findViewById(R.id.license_number_result);
        correctedLicenseNumber =findViewById(R.id.license_number_corrected);

        btnShowLicenseNumber=findViewById(R.id.btn_show_license_number);
        btnShowLicenseNumber.setEnabled(false);

        btnStoreData=findViewById(R.id.btn_store_data);
        btnStoreData.setEnabled(false);

        btnAnalizeImage=findViewById(R.id.btn_analize_image);
        btnAnalizeImage.setEnabled(false);

        btnDisplayLicensePlate =findViewById(R.id.btn_display_plate);
        btnDisplayLicensePlate.setEnabled(false);

        setAllButtonListeners();
    }

    private void setAllButtonListeners() {
        btnSelectImage.setOnClickListener((View view) -> {


            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/jpeg");

            try {
                startActivityForResult(intent, UPLOAD_REQUEST_CODE);

            } catch (ActivityNotFoundException e) {

                e.printStackTrace();
            }

        });

        btnTakePicture.setOnClickListener((View view)->{
            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

            try {
                startActivityForResult(cameraIntent, TAKE_PICTURE_REQUEST_CODE);

            } catch (ActivityNotFoundException e) {

                e.printStackTrace();
            }
        });

        btnShowLicenseNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getGeneratedLicenseNumber();
                if(!btnStoreData.isEnabled()){
                    btnStoreData.setEnabled(true);
                }
            }
        });

        btnStoreData.setOnClickListener(new View.OnClickListener() {
            LicenseNumber licenseNumber= new LicenseNumber();

            @Override
            public void onClick(View v) {
                String generatedNumber= generatedLicenseNumber.getText().toString();
                String correctedNumber = correctedLicenseNumber.getText().toString();
                licenseNumber.setRecognizedNumber(generatedLicenseNumber.getText().toString());
                if(correctedNumber.isEmpty()) {
                    licenseNumber.setCorrectedNumber(generatedNumber);
                }
                else{
                    licenseNumber.setCorrectedNumber(correctedNumber);
                }
                postLicenseNumber(licenseNumber);
                generatedLicenseNumber.setText("Result");
            }
        });

        btnAnalizeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = ImgTitle.getText().toString();
                analizeImage(title);
                btnDisplayLicensePlate.setEnabled(true);
                btnShowLicenseNumber.setEnabled(true);
            }
        });

        btnDisplayLicensePlate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getImage();
            }
        });
    }

    private void analizeImage(String title) {
        WebApiClient webApiClient = ServiceGenerator.getWebApiClient().create(WebApiClient.class);
        Call<String> call = webApiClient.analizeImage(title);
        call.enqueue(new Callback<String>() {

            @Override
            public void onResponse(Call<String> call, retrofit2.Response<String> response) {

                Toast toast;
                if (response.isSuccessful() && response.body()!=null) {
                    toast = Toast.makeText(MainActivity.this, response.body(), Toast.LENGTH_LONG);
                    toast.show();
                } else {

                    ResponseBody errorBody = response.errorBody();
                    Gson gson = new Gson();
                    try {

                        Response errorResponse = gson.fromJson(errorBody.string(), Response.class);
                        toast = Toast.makeText(MainActivity.this, errorResponse.getMessage(), Toast.LENGTH_SHORT);
                        toast.show();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

                Log.d(TAG, "onFailure: " + t.getLocalizedMessage());
                Toast toast = Toast.makeText(MainActivity.this, "Couldn't analyze image!", Toast.LENGTH_SHORT);
                toast.show();
            }
        });
    }

    private void postLicenseNumber(LicenseNumber licenseNumber) {
        WebApiClient webApiClient = ServiceGenerator.getWebApiClient().create(WebApiClient.class);
        Call<LicenseNumber> call = webApiClient.postLicenseNumber(licenseNumber);
        call.enqueue(new Callback<LicenseNumber>() {

            @Override
            public void onResponse(Call<LicenseNumber> call, retrofit2.Response<LicenseNumber> response) {

                Toast toast;
                if (response.isSuccessful() && response.body()!=null) {

                    toast = Toast.makeText(MainActivity.this, "Post successful!", Toast.LENGTH_LONG);
                    toast.show();
                } else {

                    ResponseBody errorBody = response.errorBody();

                    Gson gson = new Gson();

                    try {

                        Response errorResponse = gson.fromJson(errorBody.string(), Response.class);
                        toast = Toast.makeText(MainActivity.this, errorResponse.getMessage(), Toast.LENGTH_SHORT);
                        toast.show();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<LicenseNumber> call, Throwable t) {

                Log.d(TAG, "onFailure: " + t.getLocalizedMessage());
                Toast toast = Toast.makeText(MainActivity.this, "Post failed!", Toast.LENGTH_SHORT);
                toast.show();
            }
        });

    }

    private void getGeneratedLicenseNumber() {
        WebApiClient webApiClient = ServiceGenerator.getWebApiClient().create(WebApiClient.class);
        Call<String> call = webApiClient.getLicenseNumber();
        call.enqueue(new Callback<String>() {

            @Override
            public void onResponse(Call<String> call, retrofit2.Response<String> response) {

                Toast toast;
                if (response.isSuccessful() && response.body()!=null) {
                    generatedLicenseNumber.setText(response.body());//clear text
                    toast = Toast.makeText(MainActivity.this, "Fetch successful!", Toast.LENGTH_LONG);
                    toast.show();
                } else {

                    ResponseBody errorBody = response.errorBody();

                    Gson gson = new Gson();

                    try {

                        Response errorResponse = gson.fromJson(errorBody.string(), Response.class);
                        toast = Toast.makeText(MainActivity.this, errorResponse.getMessage(), Toast.LENGTH_SHORT);
                        toast.show();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

                Log.d(TAG, "onFailure: " + t.getLocalizedMessage());
                Toast toast = Toast.makeText(MainActivity.this, "Couldn't get result!", Toast.LENGTH_SHORT);
                toast.show();
            }
        });

    }

    /**
     * On Result, we will get the Uri of the image
     * From that Uri, we will open an InputStream and read the image contents as a byte array
     */

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Uri tempUri;
        if (requestCode == UPLOAD_REQUEST_CODE) {

            if (resultCode == RESULT_OK) {
                path = data.getData();
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), path);
                    Img.setImageBitmap(bitmap);
                    EnableUploadButton(path);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        if(requestCode== TAKE_PICTURE_REQUEST_CODE){
            if (resultCode == RESULT_OK){
                try{
                    bitmap = (Bitmap) data.getExtras().get("data");
                    path=data.getData();
                    /*
                    // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
                    tempUri = getImageUri(getApplicationContext(), bitmap);
                    // CALL THIS METHOD TO GET THE ACTUAL PATH
                    File finalFile = new File(getRealPathFromURI(tempUri));
                    Uri fileUri = Uri.fromFile(finalFile);

                    Bitmap uploadBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
                    */
                    Img.setImageBitmap(Bitmap.createScaledBitmap(bitmap, 120, 120, false));
                    EnableUploadButton(path);

                }
                catch (Exception e){
                    e.printStackTrace();
                }

            }

        }
    }
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        Bitmap OutImage = Bitmap.createScaledBitmap(inImage, 1000, 1000,true);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        String path = "";
        if (getContentResolver() != null) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                path = cursor.getString(idx);
                cursor.close();
            }
        }
        return path;
    }

    public void EnableUploadButton(Uri fileUri){
        btnUpload.setEnabled(true);

        btnUpload.setOnClickListener(view -> {
            if(fileUri!=null){
                try {
                    InputStream is = getContentResolver().openInputStream(fileUri);
                    uploadImage(getBytes(is));

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public byte[] getBytes(InputStream is) throws IOException {
        ByteArrayOutputStream byteBuff = new ByteArrayOutputStream();

        int buffSize = 1024;
        byte[] buff = new byte[buffSize];

        int len = 0;
        while ((len = is.read(buff)) != -1) {
            byteBuff.write(buff, 0, len);
        }

        return byteBuff.toByteArray();
    }


    private void uploadImage(byte[] imageBytes) {

        WebApiClient webApiClient = ServiceGenerator.getWebApiClient().create(WebApiClient.class);

        RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), imageBytes);

        MultipartBody.Part body = MultipartBody.Part.createFormData(ImgTitle.getText().toString(), ImgTitle.getText().toString()+".jpg", requestFile);

        Call<Response> call = webApiClient.uploadImage(body);

        call.enqueue(new Callback<Response>() {

            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {

                Toast toast;
                if (response.isSuccessful()) {

                    toast = Toast.makeText(MainActivity.this, "Upload Successfull!" , Toast.LENGTH_LONG);
                    toast.show();
                    btnAnalizeImage.setEnabled(true);
                } else {

                    ResponseBody errorBody = response.errorBody();

                    Gson gson = new Gson();

                    try {

                        Response errorResponse = gson.fromJson(errorBody.string(), Response.class);
                        toast=Toast.makeText(MainActivity.this, errorResponse.getMessage(), Toast.LENGTH_SHORT);
                        toast.show();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {

                Log.d(TAG, "onFailure: "+t.getLocalizedMessage());
                Toast toast=Toast.makeText(MainActivity.this, "Couldn't upload image!", Toast.LENGTH_SHORT);
                toast.show();
            }
        });
    }

    private void getImage(){
        WebApiClient webApiClient = ServiceGenerator.getWebApiClient().create(WebApiClient.class);
        Call<ResponseBody> call = webApiClient.getImage();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                Toast toast;

                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        // display the image data in a ImageView or save it
                        Bitmap bmp = BitmapFactory.decodeStream(response.body().byteStream());
                        Img.setImageBitmap(bmp);
                    } else {
                        toast = Toast.makeText(MainActivity.this, "Image is null!" , Toast.LENGTH_LONG);
                        toast.show();
                    }
                } else {
                    toast = Toast.makeText(MainActivity.this, "Could not get image!" , Toast.LENGTH_LONG);
                    toast.show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getLocalizedMessage());
                Toast toast = Toast.makeText(MainActivity.this, "Could not get image!" , Toast.LENGTH_LONG);
                toast.show();
            }
        });
    }
}
